import os
import urllib
from time import sleep
import py_eureka_client.eureka_client as eureka_client
from flask_restful import Resource

service_name = None

def register(serv_name=None, port=5000):
    # no spaces or underscores, this needs to be url-friendly
    if serv_name is None:
       print(' * Not registering discovery service. Skipping...') 
       return False

    home_page_url = 'manage/info'
    manage_health_path = 'manage/health'
    eureka_url = 'http://discovery:8761/eureka/'
    service_name = serv_name

    try:
        sleep(15)
        registry_client, discovery_client = eureka_client.init(eureka_server=eureka_url,
                                                               app_name=service_name,
                                                               instance_ip=service_name,
                                                               instance_port=port)
        registry_client.send_heart_beat()
        return True

    except Exception as e:
        return register(service_name)


class Registry(Resource):
    def get(self, param):
        if param == 'health':
            return { 'status': 'UP' }
        elif param == 'info':
            return { 'app': service_name }
        else:
            return { 'error': '404 route not found' }
