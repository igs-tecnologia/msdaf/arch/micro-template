import os
from flask import Flask, Response, send_from_directory

def create_app(config_filename):
    app = Flask(__name__, static_url_path='')
    app.config.from_object(config_filename)
    
    from app import api_bp
    app.register_blueprint(api_bp, url_prefix='/api')

    from src.models import db
    db.init_app(app)

    return app


if __name__ == "__main__":
    app = create_app('config')
    root = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'docs')

    @app.route('/docs', methods=['GET'])
    def docs():
        return send_from_directory(root, 'index.html')

    @app.route('/docs/<path>', methods=['GET'])
    def docs_path(path):
        return send_from_directory(root, path)

    app.run(debug=True)
