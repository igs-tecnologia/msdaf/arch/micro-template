# micro-template

Template for creating a micro service. Encapsulates the Eureka connector, a Flask REST API infrastructure, and an opcional Postgresql (Greenplum) connection.

Follow [this tutorial](https://bytes.com/topic/python/answers/678286-how-find-available-classes-file) for more information about building a simple RESTfull API in Flask.
See also [Flask](https://palletsprojects.com/p/flask/) official docs to get started.
